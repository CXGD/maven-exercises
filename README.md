# MAVEN企业实战

### 课程目的

- 完成Maven的环境配置
- 完成基于IDEA的项目构建
    - 自定义配置文件
    - 多配置文件
    - 单元测试
    - 修改terminal为cmd模式
- 完成修改maven的镜像仓库
    - 修改为华为云镜像
- 基于maven测试junit5的项目
    - 添加junit5插件
- 完成maven项目打包，并指定端口启动
- 完成maven项目打包，并指定配置文件启动

### 环境配置

#### 1、下载maven

https://maven.apache.org/download.cgi

> 由于maven与idea存在兼容问题，需要下载的版本不宜太新，建议使用3.6.3

#### 2、配置环境

##### 需要配置JAVA_HOME

> 机房电脑的JAVA_HOME存在问题，**需要删除之后**，在重新新建JAVA_HOME

##### 需要配置MAVEN_HOME

##### 需要配置MAVEN的path

#### 3、修改maven的settings.xml配置华为云

```xml
<!-- 配置华为的云服务 -->
 <server>
  <id>huaweicloud</id>
  <privateKey>anonymous</privateKey>
  <passphrase>devcloud</passphrase>
</server>
```

```XML
 <!-- 配置华为的Maven镜像 -->
<mirror>
   <id>huaweicloud</id>
   <mirrorOf>*</mirrorOf>
  <url>https://mirrors.huaweicloud.com/repository/maven/</url>
 </mirror>
```

> 修改配置文件前建议备份一份源文件，并且采用编辑软件打开，尽可能不用windows自带的文本编辑器

### 创建SpringBoot项目

#### 修改IDEA的MAVEN配置

> 注意新版的IDEA需要修改两个地方，一个是本项目，一个是其他项目

#### 1、创建项目

> 选择java web 依赖即可

#### 2、复制配置文件

改为application-dev.properties,端口8081

改为application-test.properties,端口8082

改为application-prod.properties,端口8083

#### 3、新建自定义配置参数

随机创建参数，使用@Value("${xxx.xxx}")获取

#### 4、创建控制层访问

最简单的方式，直接在启动类上添加

@RestController

并新增一个方法

@GetMapping







#### 5、使用IDEA自带的maven进行打包package

#### 6、添加maven test的插件

```
<plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.19.1</version>
                <configuration>
                    <skipTests>false</skipTests>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.platform</groupId>
                        <artifactId>junit-platform-surefire-provider</artifactId>
                        <version>1.1.0</version>
                    </dependency>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-engine</artifactId>
                        <version>5.1.0</version>
                    </dependency>
                </dependencies>
            </plugin>
```

> - 注意单元测试识别的类必须是Test结尾
> - 编写若干单元测试案例
> - 修改terminal为cmd模式
> - 完成 mvn test
> - 完成mvn clean package
> - 完成mvn clean package -Dmave.test.skip=true
> - 完成java -jar xxx.jar --server.port = 8888
> - 完成java -jar xxx.jar --spring.profiles.active=prod