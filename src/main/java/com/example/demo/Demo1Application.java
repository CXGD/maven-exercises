package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class Demo1Application {

    @Value("${xx.xx.xx}")
    private String name;

    @GetMapping
    public String index(){
        return name;
    }

    public static void main(String[] args) {
        SpringApplication.run(Demo1Application.class, args);
    }

}
